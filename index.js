module.exports = function (schema, options) {
    var fs = require('fs')
    var path = require('path');

    const opts = options || {};

    let stream, schemaName, streamPath, dbLogging, consoleLogging;

    streamPath = opts.path || "mongoose.log";
    dbLogging = opts.database || false;
    consoleLogging = opts.console || false;
    schemaName = opts.schema || ""


    stream = fs.createWriteStream(path.join(streamPath), { flags: 'a' });

    // create action logs
    schema.post('save', function (doc, next) {
        writeStream(`[${getTime()}] "save ${schemaName}" ${JSON.stringify({ data: this._doc })} \n`);
        if (consoleLogging) { writeConsole(); }
        return next();

    });

    // update action logs
    schema.post('update', function (doc, next) {

    });

    schema.post('findOneAndUpdate', function (doc, next) {

    });

    // create logs for delete action
    schema.post('findOneAndRemove', function (doc, next) {

    });

    schema.post('remove', function (doc, next) {

    });

    function writeStream(content) {
        if (stream) {
            stream.write(content);
        }
    }

    function writeDatabase() {

    }

    function writeConsole() {
        console.log(`[${getTime()}] "save ${schemaName}" \n`)
        //console.log(`[${getTime()}] "save ${schemaName}" ${JSON.stringify({ data: this._doc })} \n`)
    }

    function getTime() {
        let dateTime = new Date();
        var date = dateTime.getUTCDate();
        var hour = dateTime.getUTCHours();
        var mins = dateTime.getUTCMinutes();
        var secs = dateTime.getUTCSeconds();
        var month = dateTime.getUTCMonth();
        var year = dateTime.getUTCFullYear();

        return pad2(date) + '/' + getMonth(month) + '/' + year +
            ':' + pad2(hour) + ':' + pad2(mins) + ':' + pad2(secs) +
            ' +0000'

    }

    function pad2(num) {
        var str = String(num)

        // istanbul ignore next: num is current datetime
        return (str.length === 1 ? '0' : '') + str
    }

    function getMonth(i) {
        return [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        ][i];
    }
}